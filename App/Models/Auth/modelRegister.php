<?php 

require_once("App/Models/model.php");

 class  modelRegister extends model
 {
private $name ,$firstName,$email,$password,$compte_type;

        function __construct($post){
            
            /////inialiser les valeurs des champs de formulaire

            if(!empty($post['name']))$this->name= (string) $post['name'];
            if(!empty($post['firstName']))$this->firstName= (string) $post['firstName'];
            if(!empty($post['email']))$this->email=(string) $post['email'];
            ///hasher le password 
            if(!empty($post['password']))$this->password=(string) md5($post['password']);
            ///
            if(!empty($post['compte_type']))$this->compte_type=(string) $post['compte_type']; 
        
        }

        public function RegisteUser(){
            
            //connexion a la BDD
            $bdd=$this->getBdd();
                
            ///insert la requete si tout est parfait
            $req="INSERT INTO users(nom,prenom,email,password,create_at,modification_at,compte_type) VALUES (?,?,?,?,NOW(),NOW(),'$this->compte_type')";
            
            $requete=$bdd->prepare($req);

            $requete->execute(array($this->name,$this->firstName,$this->email,$this->password));
            
            return $bdd->lastInsertId();


        }

        ///les Getters

        public function getName(){
            return (string) $this->name;
        }
            public function getFirstName(){
               return (string) $this->firstName;
            }
                public function getEmail(){
                    return (string) $this->email;
                }
                    public function getPasswordHash(){
                        return (string) $this->password;
                    }
                        public function getCompte_type(){
                            return (string) $this->compte_type;
                        }

        

 }
